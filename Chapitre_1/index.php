<!DOCTYPE html>
<html>
  <head>
    <!-- lien du css -->
    <link rel="stylesheet" type="text/css" href="main.css" />
    <!-- lien du BootstrapCDN -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- lien des plugins Jquery pour Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <meta charset="utf-8" />
  </head>

  <!-- Bootstrap découpe l'écran en 12 colonnes sur une nombre illimité de lignes -->
  <!-- L'interieur de chaques "row" (lignes) se redécoupera en 12 colonnes -->

  <body>
    <div class="container-fluid"> <!-- Il faut toujours un container -->
      <div class="row"> <!-- 1e ligne -->
        <div class="col-md-6">6 colonnes <!-- 1e ligne: une div de 6 colonnes -->
          <div class="row"> <!-- 2e ligne -->
            <div class="col-md-4">4 colonnes</div> <!-- 2e ligne: ligne une div de 4 colonnes -->
            <div class="col-md-offset-4 col-md-4">4 colonnes</div> <!-- 2e ligne: une div de 4 colonnes en sautant 4 colonnes au milieu -->
          </div>
        </div>
        <div class="col-md-6">6 colonnes <!-- 1e ligne: une div de 6 colonnes -->
          <div class="row"> <!-- 2e ligne -->
            <div class="col-md-4">4 colonnes</div> <!-- 2e ligne: une div de 4 colonnes -->
            <div class="col-md-8">8 colonnes</div> <!-- 2e ligne: une div de 8 colonnes -->
          </div>
        </div>
        <div class="row"> <!-- 3e ligne -->
          <div class="col-md-offset-4 col-md-4">Bonjour <!-- 3eme ligne: une div espacé de 4 colonnes et qui fait 4 colonnes -->
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
