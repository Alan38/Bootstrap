<!DOCTYPE html>
<html>
    <head>
        <!-- lien du css -->
        <link rel="stylesheet" type="text/css" href="main.css" />
        <!-- lien du BootstrapCDN -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- lien des plugins Jquery pour Bootstrap -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <meta charset="utf-8" />
    </head>

    <body>
        <main class="container-fluid">
            <header class="row">
                <section class="col-sm-12">
                    Entête
                </section>
            </header>

            <section class="row">
                <nav class="col-sm-2">
                    Menu
                </nav>
                <section class="col-sm-10">
                    Section1
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="row">
                                <article class="col-sm-6">
                                    Article1
                                </article>
                                <article class="col-sm-6">
                                    Article2
                                </article>
                                <article class="col-sm-6">
                                    Article3
                                </article>
                                <article class="col-sm-6">
                                    Article4
                                </article>
                                <article class="col-sm-6">
                                    Article5
                                </article>
                                <article class="col-sm-6">
                                    Article6
                                </article>
                            </div>
                        </div>
                        <aside class="col-sm-2">
                            <div class="row">
                                <article class="col-sm-12">
                                    Aside
                                </article>
                                <article class="col-sm-12">
                                    Aside
                                </article>
                            </div>
                        </aside>
                    </div>
                </section>
                <section class="col-sm-offset-2 col-sm-10">
                    Section2
                    <div class="row">
                        <article class="col-sm-6">
                            Article7
                        </article>
                        <article class="col-sm-6">
                            Article8
                        </article>
                        <article class="col-sm-6">
                            Article9
                        </article>
                        <article class="col-sm-6">
                            Article10
                        </article>
                        <article class="col-sm-6">
                            Article11
                        </article>
                        <article class="col-sm-6">
                            Article12
                        </article>
                    </div>
                </section>
                <footer class="col-sm-12">
                    Pied de page
                </footer>
            </section>
        </main>
    </body>
</html>
