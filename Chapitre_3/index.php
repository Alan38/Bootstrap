<!DOCTYPE html>
<html>

    <head>
        <!-- lien du css -->
        <link rel="stylesheet" type="text/css" href="main.css" />
        <!-- lien du BootstrapCDN -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- lien des plugins Jquery pour Bootstrap -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <meta charset="utf-8" />
    </head>

    <body>
        <main class="container-fluid">
            <header class="row">
                <article class="col-sm-12 col-md-12">
                    Entête
                </article>
            </header>

            <section class="row">
                <section class="col-sm-2 col-md-2">
                    <article class="row">
                        <aside class="col-sm-12 col-md-12">
                            Aside1
                        </aside>
                        <aside class="col-sm-12 col-md-12">
                            Aside2
                        </aside>
                    </article>
                </section>

                <section class="col-sm-10 col-md-8">
                    Section<br />
                    Section (encore)<br />
                    Section (encore et encore)
                </section>

                <section class="col-sm-12 col-md-2">
                    <article class="row">
                        <aside class="col-sm-12 col-md-12">
                            Aside3
                        </aside>
                        <aside class="col-sm-12 col-md-12">
                            Aside4
                        </aside>
                    </article>
                </section>
            </section>

            <footer class="row">
                <article class="col-sm-12 col-md-12">
                    Pied de page
                </article>
            </footer>
        </main>
    </body>
</html>
