<!DOCTYPE html>
<html>

    <head>
        <!-- lien du css -->
        <link rel="stylesheet" type="text/css" href="main.css" />
        <!-- lien du BootstrapCDN -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- lien des plugins Jquery pour Bootstrap -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <meta charset="utf-8" />
        <style type="text/css">
            .col-md-2 {
                line-height: 100px;
            }
            .col-md-5 {
                line-height: 200px;
            }
            .col-md-12 {
                line-height: 80px;
            }
        </style>
    </head>

    <body>
        <header>
            <div class="visible-xs col-md-12">
                Entête 768px-
            </div>
            <div class="visible-sm col-md-12">
                Entête 768px+
            </div>
            <div class="visible-md col-md-12">
                Entête 991px+
            </div>
            <div class="visible-lg col-md-12">
                Entête 1200px+
            </div>
        </header>

        <main class="container">
            <section class="row">
                <nav class="col-sm-12 col-md-2">
                    Navigation
                </nav>
                <article class="col-sm-6 col-md-5">
                    Section
                </article>
                <article class="col-sm-6 col-md-5">
                    Section
                </article>
            </section>

            <section class="row">
                <footer class="col-sm-12 col-md-12">
                    Pied de page
                </footer>
            </section>
        </main>
    </body>
</html>
