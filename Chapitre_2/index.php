<!DOCTYPE html>
<html>

    <head>
        <!-- lien du css -->
        <link rel="stylesheet" type="text/css" href="main.css" />
        <!-- lien du BootstrapCDN -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- lien des plugins Jquery pour Bootstrap -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <meta charset="utf8" />
    </head>

    <body>
        <main class="container-fluid">
            <header class="col-md-12 page-header">
                <h1>Les lions domestiques disparus, RT si t'es triste :'(</h1>
            </header>
            <section class="row">
                <article class="col-md-12">
                    <p>
                        Waouh y'a trop plein de <strong>Lions</strong> ici, tu pêux t'éclater a regarder
                        tout ces petits (LOL) lions tout mignons. J'ai crée ce site pour
                        l'asociation <abbr title="Société des Bites avec Engelures">SBE</abbr>,
                        amusez-vous bien a matter ces petits liions et n'éhistez pas a vous toucher
                        dessus !
                    </p>
                    <p>
                        Voici ce qu'en dit le Wikipédestre :
                    </p>
                    <blockquote>
                        Le lion (Panthera leo) est une espèce de mammifères carnivores de
                        la famille des félidés. La femelle du lion est la lionne, son
                        petit est le lionceau. Le mâle adulte, aisément reconnaissable
                        à son importante crinière, accuse une masse moyenne qui peut
                        être variable selon les zones géographiques où il se trouve,
                        allant de 180 kg pour les lions de Kruger à 230 kg pour les
                        lions de Transvaal. Certains spécimens très rares peuvent
                        dépasser exceptionnellement 250 kg. Un mâle adulte se nourrit
                        de 7 kg de viande chaque jour contre 5 kg chez la femelle. Le
                        lion est un animal grégaire, c'est-à-dire qu'il vit en larges
                        groupes familiaux, contrairement aux autres félins. Son espérance
                        de vie, à l'état sauvage, est comprise entre 7 et 12 ans pour
                        le mâle et 14 à 20 ans pour la femelle, mais il dépasse fréquemment
                        les 30 ans en captivité.

                        Le lion mâle ne chasse qu'occasionnellement, il est chargé de
                        combattre les intrusions sur le territoire et les menaces
                        contre la troupe. Le lion rugit. Il n'existe plus à l'état
                        sauvage que 16 500 à 30 000 individus dans la savane africaine,
                        répartis en une dizaine de sous-espèces et environ 300 au parc
                        national de Gir Forest dans le nord-ouest de l'Inde. Il est
                        surnommé « le roi des animaux » car sa crinière lui donne un
                        aspect semblable au Soleil, qui apparaît comme « le roi des
                        astres ». Entre 1993 et 2017, leur population a baissé de 43%.<br />
                        <small class="pull-right">Wikipédestre</small><br />
                        <!-- L'élément small permet de mettre un texte en petit et grisaillé -->
                    </blockquote>
                </article>
            </section>
            <section class="row">
                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"><img src="1.jpg" alt="Lion"/></div>
                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"><img src="2.jpg" alt="Lion" /></div>
                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"><img src="3.jpg" alt="Lion" /></div>
                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"><img src="4.jpg" alt="Lion" /></div>
                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"><img src="5.jpg" alt="Lion" /></div>
                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"><img src="6.jpg" alt="Lion" /></div>
                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"><img src="7.jpg" alt="Lion" /></div>
                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"><img src="8.jpg" alt="Lion" /></div>
                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"><img src="9.jpg" alt="Lion" /></div>
                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"><img src="10.jpg" alt="Lion" /></div>
                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"><img src="11.jpg" alt="Lion" /></div>
                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"><img src="12.jpg" alt="Lion" /></div>
            </section>
            <section class="row">
                <adress class="col-sm-4 col-md-4">
                    <p>
                        Vous pouvez me contacter a cette adresse pour plus de renseignements
                        à propos des Lions disparus :
                    </p>
                    <p>
                        Alan Folklorique<br />
                        Allée du sex-appeal,<br />
                        27589 Okalm-sur-Seine.
                    </p>
                </adress>
                <article class="col-sm-8 col-md-8">
                    <img src="13.jpg" alt="Lion"/>
                </article>
            </section>
        </main>
    </body>
</html>
